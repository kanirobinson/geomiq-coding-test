<?php

use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    protected $roles = [
        [
            'name' => 'admin',
            'created_at' => '2017-04-08 16:53:43',
        ],
        [
            'name' => 'vendor',
            'created_at' => '2017-04-20 14:59:21',
        ],
    ];

    /**
     * @var array
     */
    protected $users = [
        [
            'name' => 'Viva Ratke',
            'email' => 'viva.ratke@gmail.com',
            'password' => 'password123',
            'role' => 'admin',
            'created_at' => '2018-04-08 16:53:43',
            'last_login' => '2019-08-10 09:11:34',
            'profile' => [
                'company_name' => 'Hartmann-Wiegand',
                'created_at' => '2018-04-08 16:53:43',
            ]
        ],
        [
            'name' => 'Candelario Rempel',
            'email' => 'c.rempel@hotmail.com',
            'password' => 'password123',
            'role' => 'vendor',
            'created_at' => '2018-04-20 14:59:21',
            'last_login' => '2019-09-12 16:20:55',
            'profile' => [
                'company_name' => 'Mertz-Bradtke',
                'created_at' => '2018-04-20 14:59:21',
            ]
        ],
        [
            'name' => 'Nelson Powlowski',
            'email' => 'nelson.pow@gmail.com',
            'password' => 'password123',
            'created_at' => '2018-05-29 15:14:50',
            'last_login' => '2019-09-12 10:66:12',
            'profile' => [
                'company_name' => 'Kertzmann LLC',
                'created_at' => '2018-05-29 15:14:50',
            ]
        ],
        [
            'name' => 'Myrtis Klein',
            'email' => 'myrtis@yahoo.com',
            'password' => 'password123',
            'created_at' => '2018-07-15 13:08:57',
            'last_login' => '2019-09-13 09:42:21',
            'profile' => [
                'company_name' => 'Wilderman-Heller',
                'created_at' => '2018-07-15 13:08:57',
            ]
        ],
    ];

    /**
     * @return void
     */
    public function run(): void
    {
        foreach ($this->roles as $role) {
            Role::firstOrCreate(['name' => $role['name']]);
        }

        foreach ($this->users as $u) {
            $user = User::firstOrCreate(['email' => $u['email']], [
                'name' => $u['name'],
                'password' => Hash::make($u['password']),
                'last_login' => $u['last_login'],
            ]);

            $user
                ->profile()
                ->create([
                    'company_name' => $u['profile']['company_name'],
                ]);

            if (isset($u['role'])) {
                $role = Role::whereName($u['role'])->first();
                $user->role()->save($role);
            }
        }
    }
}

<?php

namespace App\Services;

class Formatter
{
    /**
     * @var string
     */
    private $string = '';

    /**
     * @var array
     */
    private $exploded = [];

    /**
     * @var array
     */
    private $formatted = [];

    /**
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
        $this->string = preg_replace('/(\/\/)/', '=', $this->string);
        $this->exploded = preg_split('/(,|%)/', $this->string);
    }

    /**
     * @return self
     */
    public function format(): self
    {
        foreach ($this->exploded as $data) {
            $this->parseElapsedTime($data);
            $this->parseType($data);
            $this->parseFeatures($data);
        }

        return $this;
    }

    /**
     * @param string $data
     * @return void
     */
    private function parseElapsedTime(string $data): void
    {
        if (preg_match('/elapsed_time=([0-9\.]+)/im', $data, $matches)) {
            $this->formatted['elapsed_time'] = round($matches[1], 6);
        }
    }

    /**
     * @param string $data
     * @return void
     */
    private function parseType(string $data): void
    {
        if (preg_match('/type-(\w+)/im', $data, $matches)) {
            $this->formatted['type'] = $matches[1];
        }
    }

    /**
     * @param string $data
     * @return void
     */
    private function parseFeatures(string $data): void
    {
        if (preg_match('/position-(\d+)=([0-9\.]+)/im', $data, $matches)) {
            $this->setFeature($matches, 'position');
            array_push($this->formatted['features'][$matches[1]]['position'], (int) $matches[2]);
        }

        if (preg_match('/radius-(\d+)-([0-9\.]+)/im', $data, $matches)) {
            $this->setFeature($matches);
            $this->formatted['features'][$matches[1]]['radius'] = $matches[2];
        }

        if (preg_match('/direction-(\d+)=(.*)/im', $data, $matches)) {
            $this->setFeature($matches);
            $this->formatted['features'][$matches[1]]['direction'] = $matches[2];
        }
    }

    /**
     * @param array $matches
     * @param string|null $key
     * @return void
     */
    private function setFeature(array $matches, ?string $key = null): void
    {
        $featureId = $matches[1];

        if (!isset($this->formatted['features'][$featureId])) {
            $this->formatted['features'][$featureId] = [
                'id' => $featureId,
            ];
        }

        if (!is_null($key)) {
            if (!isset($this->formatted['features'][$featureId][$key])) {
                $this->formatted['features'][$featureId][$key] = [];
            }
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $additionalData = [];

        if (isset($this->formatted['features']) && is_array($this->formatted['features'])) {
            $additionalData['feature_count'] = count($this->formatted['features']);
        }

        return array_merge($this->formatted, $additionalData);
    }
}

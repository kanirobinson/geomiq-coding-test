<?php

namespace App\Console\Commands;

use App\Services\Formatter;
use Illuminate\Console\Command;

class PartACommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'part:a';

    /**
     * @var string
     */
    protected $description = 'A command for part a of the test.';

    /**
     * @var string
     */
    protected $string = 'elapsed_time=0.0022132396697998047,type-CNC, radius-1-15,position-1=0.000000000000014,position-1//90,position-2=0%direction-1=-2.0816681711721685e-16';

    /**
     * @return mixed
     */
    public function handle()
    {
        $formatted = (new Formatter($this->string))
            ->format()
            ->toArray();

        dd($formatted);
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PartBCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'part:b';

    /**
     * @var string
     */
    protected $description = 'A command to retrieve users from the database with their role and company name.';

    /**
     * @return mixed
     */
    public function handle()
    {
        $results = DB::select(DB::raw("
            SELECT
                `users`.`id` AS `id`,
                `users`.`name` AS `name`,
                IFNULL(`roles`.`name`, 'buyer') AS `role`,
                `users`.`email` AS `email`,
                `profiles`.`company_name` AS `company_name`,
                `users`.`created_at` AS `registered_on`,
                `users`.`last_login` AS `last_login`
            FROM
                `users`
                LEFT JOIN `profiles` ON `profiles`.`user_id` = `users`.`id`
                LEFT JOIN `model_roles` ON `model_roles`.`model_id` = `users`.`id`
                LEFT JOIN `roles` ON `roles`.`id` = `model_roles`.`role_id`
        "));

        dd($results);
    }
}
